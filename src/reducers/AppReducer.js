import { DataReducer } from './DataReducer'
import { MenuReducer } from './MenuReducer'

export const AppReducer = {
   DataReducer: DataReducer,
   MenuReducer: MenuReducer
}