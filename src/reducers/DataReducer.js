import { SAVE_TODO, SAVE_PROGRESS, SAVE_DONE, SAVE_DELETE } from '../types/types'

const initialState = {
   todo     : [],
   progress : [],
   done     : []
}

export const DataReducer = (state = initialState, action) => {
   switch(action.type) {
      case SAVE_TODO:
         return {
            ...state,
            todo: action.data
         }

      case SAVE_PROGRESS:
         return {
            ...state,
            progress: action.data
         }

      case SAVE_DONE:
         return {
            ...state,
            done: action.data
         }

      case SAVE_DELETE:
         return {
            ...state,
            todo     : [],
            progress : [],
            done     : []
         }

      default:
         return state
   }
}