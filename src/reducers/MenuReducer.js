import { MENU_CHANGE } from '../types/types'

const initialState = {
   data: [ 'Todo', 'On Progress', 'Done' ]
}

export const MenuReducer = (state = initialState, action) => {
   switch(action.type) {
      case MENU_CHANGE:
         return {
            ...state,
            data: action.data
         }

      default:
         return state
   }
}