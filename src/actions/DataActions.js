import * as types from '../types/types'

export const saveTodo = (arrData) => {
   return {
      ...{ type: types.SAVE_TODO },
      ...arrData
   }
}

export const saveProgress = (arrData) => {
   return {
      ...{ type: types.SAVE_PROGRESS },
      ...arrData
   }
}

export const saveDone = (arrData) => {
   return {
      ...{ type: types.SAVE_DONE },
      ...arrData
   }
}

export const saveDelete = () => {
   return {
      type: types.SAVE_DELETE
   }
}