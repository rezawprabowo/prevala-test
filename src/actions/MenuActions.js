import * as types from '../types/types'

export const menuChange = (arrData) => {
   return {
      ...{ type: types.MENU_CHANGE },
      ...arrData
   }
}