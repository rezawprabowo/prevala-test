import { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'

import './styles/App.scss'

import { saveTodo, saveProgress, saveDone, saveDelete } from './actions/DataActions'
import { menuChange } from './actions/MenuActions'

import { IoAdd, IoCheckmark, IoClose, IoRemove, IoPencil } from 'react-icons/io5'

const App = (props: any) => {
   const [ menus, setMenus ] = useState<string[]>([])
   const [ menus2, setMenus2 ] = useState<any>([])
   const [ todo, setTodo ] = useState<string[]>([])
   const [ progress, setProgress ] = useState<string[]>([])
   const [ done, setDone ] = useState<string[]>([])
   const [ inputs, setInputs ] = useState<string>('')
   const [ newTitle, setNewtitle ] = useState<string>('')
   const [ newContent, setNewcontent ] = useState<string>('')
   const [ active, setActive ] = useState<number>(-1)
   const [ percentage, setPercentage ] = useState<number>(0)
   const [ titleStatus, setTitlestatus ] = useState<number>(-1)
   const [ editing, setEditing ]  = useState<number>(-1)
   const [ loading, setLoading ] = useState<boolean>(false)
   const [ dragData, setDragdata ] = useState<any>()

   useEffect(() => {
      setLoading(false)
   }, [loading])

   useEffect(() => {
      setInputs('')
   }, [active])

   useEffect(() => {
      if(todo.length > 0) {
         props.saveTodo({ data: todo })
      }
      if(progress.length > 0) {
         props.saveProgress({ data: progress })
      }
      if(done.length > 0) {
         props.saveDone({ data: done })
      }

      let totalData  = todo.length + progress.length + done.length
      let unfinished = todo.length + progress.length
      let percentage = (totalData - unfinished) / totalData * 100

      setPercentage(percentage)
   }, [todo, progress, done])

   useEffect(() => {
      setMenus(props.menus.data)
      setMenus2(props.menus)
   }, [props.menus])

   useEffect(() => {
      let datas = props.saves
               
      setTodo(datas.todo || todo)
      setProgress(datas.progress || progress)
      setDone(datas.done || done)
   }, [props.saves])

   function reset() {
      setTodo([])
      setProgress([])
      setDone([])
      
      props.saveDelete()
   }

   // edit title
   function editTitle(id: number) {
      setInputs('')
      setActive(-1)
      setTitlestatus(id)
      setNewtitle(menus[id])
   }

   function saveTitle(id: number) {
      let temp = menus2
      temp.data[id] = newTitle

      setMenus(temp.data)
      props.menuChange(temp)
      setTitlestatus(-1)
   }

   // add content
   function addContent(id: number, drag: string | null) {
      if(inputs === '' && !drag) {
         alert('Content cannot be empty')
      }
      else {
         if(id === 0) {
            setTodo([ ...todo, drag ? dragData['val'] : inputs ])
         }
         else if(id === 1) {
            setProgress([ ...progress, drag ? dragData['val'] : inputs ])
         }
         else if(id === 2) {
            setDone([ ...done, drag ? dragData['val'] : inputs ])
         }

         setActive(-1)
      }
   }

   // delete content
   function deleteContent(id: number, index: number) {
      let temp = id === 0 ? todo : (id === 1 ? progress : done)
      temp.splice(index, 1)

      if(id === 0) {
         setTodo(temp)
      }
      else if(id === 1) {
         setProgress(temp)
      }
      else if(id === 2) {
         setDone(temp)
      }
      
      setLoading(true)
   }

   // edit content
   function confirmEdit(id: number, index: number) {
      if(newContent === '') {
         alert('Content cannot be empty')
      }
      else {
         if(id === 0) {
            let temp = todo
   
            temp[index] = newContent
   
            setTodo(temp)
         }
         else if(id === 1) {
            let temp = progress
   
            temp[index] = newContent
   
            setProgress(temp)
         }
         else if(id === 2) {
            let temp = done
   
            temp[index] = newContent
            
            setDone(temp)
         }
   
         setEditing(-1)
      }
   }

   // saving the Data from the Dragged component
   function dragStart(source: number, val: string, index: number) {
      setDragdata({ source: source, val: val, index: index })
   }

   // injecting the Dragged Data to destination
   // by deleting data from source and then inserting the data
   function dataChange(id: number) {
      deleteContent(dragData['source'], dragData['index'])
      addContent(id, dragData['val'])
   }

   // head component
   const BoxHeader = (props: { idx: number, title: string }) => {
      const { idx, title } = props

      return (
         <div className='boxes'>
            <div className='flex-one'>
               { title }
            </div>

            <IoPencil className='border p-1 mr-1 pointer' onClick={() => editTitle(idx)} />
            <IoAdd className='border p-1 pointer' onClick={() => setActive(idx)} />
         </div>
      )
   }

   const ProgressBar = () => {
      return (
         <div className='progress-bar'>
            <div style={{ width: `${ percentage }%` }} className='filler' />
         </div>
      )
   }

   return (
      <main className='container-column' style={{ height: '100vh' }}>
         <div className='container scroll flex-one'>
            <div className='text-center mt-2 mb-1'>
               Progress Board
            </div>

            <div className='button-reset mb-3'>
               <button className='pointer border radius bg-white' onClick={() => reset()}>
                  Reset
               </button>
            </div>

            <div className='container-boxes pb'>
               { menus.map((o, index) => (
                  <div
                     key={ index }
                     className='container-stacker radius p-3 border bg-white'
                     onDragOver={(e) => e.preventDefault()}
                     onDrop={() => dataChange(index)}
                  >
                     { titleStatus === index ?
                        <div className='boxes'>
                           <input
                              className='flex-one'
                              type='text' value={ newTitle }
                              onChange={(e) => setNewtitle(e.target.value)}
                           />

                           <IoCheckmark className='border p-1 pointer mh' onClick={() => saveTitle(index)} />
                           <IoClose className='border p-1 pointer' onClick={() => setTitlestatus(-1)} />
                        </div>
                        :
                        <BoxHeader
                           idx={ index }
                           title={ o + '(' + (index === 0 ? todo.length : index === 1 ? progress.length : done.length) + ')' }
                        />
                     }

                     { active === index &&
                        <div className='container-flex mv'>
                           <input
                              className='flex-one'
                              type='text' value={ inputs }
                              onChange={(e) => setInputs(e.target.value)}
                           />
         
                           <IoCheckmark className='border p-1 pointer mh' onClick={() => addContent(index, null)} />
                           <IoClose className='border p-1 pointer' onClick={() => setActive(-1)} />
                        </div>
                     }

                     { !loading &&
                        <div className='container-column'>
                           { index === 0 ?
                              todo.map((o, index) => (
                                 <div
                                    key={ index }
                                    className={ 'container-content border' }
                                    draggable={ true }
                                    onDragStart={() => dragStart(0, o, index)}
                                 >
                                    { editing === index ?
                                       <>
                                          <input
                                             className='flex-one'
                                             type='text' value={ newContent }
                                             onChange={(e) => setNewcontent(e.target.value)}
                                          />
                        
                                          <IoCheckmark className='border p-1 pointer mh' onClick={() => confirmEdit(0, index)} />
                                          <IoClose className='border p-1 pointer' onClick={() => setEditing(-1)} />
                                       </>
                                       :
                                       <>
                                          <span className='flex-one'>
                                             { o }
                                          </span>
                        
                                          <IoPencil className='border p-1 pointer bg-white mr-1' onClick={() => { setNewcontent(o); setEditing(index) }} />
                                          <IoRemove className='border p-1 pointer bg-white' onClick={() => deleteContent(0, index)} />
                                       </>
                                    }
                                 </div>
                                 ))
                              :
                              index === 1 ?
                                 progress.map((o, index) => (
                                    <div
                                       key={ index }
                                       className='container-content border'
                                       draggable={ true }
                                       onDragStart={() => dragStart(1, o, index)}
                                    >
                                       { editing === index ?
                                          <>
                                             <input
                                                className='flex-one'
                                                type='text' value={ newContent }
                                                onChange={(e) => setNewcontent(e.target.value)}
                                             />
                           
                                             <IoCheckmark className='border p-1 pointer mh' onClick={() => confirmEdit(1, index)} />
                                             <IoClose className='border p-1 pointer' onClick={() => setEditing(-1)} />
                                          </>
                                          :
                                          <>
                                             <span className='flex-one'>
                                                { o }
                                             </span>
                           
                                             <IoPencil className='border p-1 pointer bg-white mr-1' onClick={() => { setNewcontent(o); setEditing(index) }} />
                                             <IoRemove className='border p-1 pointer bg-white' onClick={() => deleteContent(1, index)} />
                                          </>
                                       }
                                    </div>
                                    ))
                                 :
                                 done.map((o, index) => (
                                    <div
                                       key={ index }
                                       className='container-content border'
                                       draggable={ true }
                                       onDragStart={() => dragStart(2, o, index)}
                                    >
                                       { editing === index ?
                                          <>
                                             <input
                                                className='flex-one'
                                                type='text' value={ newContent }
                                                onChange={(e) => setNewcontent(e.target.value)}
                                             />
                           
                                             <IoCheckmark className='border p-1 pointer mh' onClick={() => confirmEdit(2, index)} />
                                             <IoClose className='border p-1 pointer' onClick={() => setEditing(-1)} />
                                          </>
                                          :
                                          <>
                                             <span className='flex-one'>
                                                { o }
                                             </span>
                           
                                             <IoPencil className='border p-1 pointer bg-white mr-1' onClick={() => { setNewcontent(o); setEditing(index) }} />
                                             <IoRemove className='border p-1 pointer bg-white' onClick={() => deleteContent(2, index)} />
                                          </>
                                       }
                                    </div>
                                    ))
                           }
                        </div>
                     }
                  </div>
                  ))
               }
            </div>
         </div>

         <ProgressBar />
      </main>
   )
}

App.propTypes = {
   saveTodo       : PropTypes.func.isRequired,
   saveProgress   : PropTypes.func.isRequired,
   saveDone       : PropTypes.func.isRequired,
   saveDelete     : PropTypes.func.isRequired,
   menuChange     : PropTypes.func.isRequired,
   menus          : PropTypes.object.isRequired,
   saves          : PropTypes.object.isRequired
}

const mapStateToProps = (state: any) => {
   return {
      menus: state.MenuReducer,
      saves: state.DataReducer
   }
}

const mapDispatchToProps = (dispatch: any) => (
   bindActionCreators({ saveTodo, saveProgress, saveDone, saveDelete, menuChange }, dispatch)
)

const Main = connect(mapStateToProps, mapDispatchToProps)(App)

export default Main