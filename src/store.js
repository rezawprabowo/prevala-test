// IMPORT PACKAGE REFERENCES

import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk'
import { createPromise } from 'redux-promise-middleware'
import { AppReducer } from './reducers/AppReducer'

const config = {
   key: 'root',
   storage,
   blacklist: [],
   debug: false
}

const reducers = persistCombineReducers(config, AppReducer)
const enhancers = [applyMiddleware(thunk, createPromise())]
const persistConfig = { enhancers }

const store = createStore(reducers, undefined, compose(...enhancers))
const persistor = persistStore(store, persistConfig, () => {
   // console.log('Test', store.getState())
})

export const createAppStore = () => {
   return { persistor, store }
}